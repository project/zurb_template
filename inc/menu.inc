<?php

use Drupal\Core\Template\Attribute;
use Drupal\Core\Url;
use Drupal\Core\Link;

/**
 * Implements hook_preprocess_menu_local_task().
 */
function zurb_template_preprocess_menu_local_task(array &$variables) {
  $variables['element']['#link']['url']->setOption('attributes', ['class'=> ['button', (!empty($variables['is_active']) ? 'active' : 'secondary')]]);
}

/**
 * Implements theme_preprocess_menu().
 */
function zurb_template_preprocess_menu(&$variables) {
  $variables['attributes']['class'][] = 'menu';
}

/**
 * Implements theme_preprocess_menu__MENU_NAME().
 */
function zurb_template_preprocess_menu__main(&$variables) {
  if ($variables['top_bar'] = theme_get_setting('zurb_template_top_bar_enable')) {
    $top_bar_attributes = new Attribute();

    if (theme_get_setting('zurb_template_top_bar_sticky')) {
      $top_bar_attributes->addClass('sticky', 'large-12');
      $top_bar_attributes->setAttribute('data-sticky', TRUE);
      $top_bar_attributes->setAttribute('data-top-anchor', 'top-bar-sticky-container');
      // 9999999(px) is used as no other bottom anchor stopped the top-bar from
      // jumping back to the top of the screen after scrolling past 50% of the
      // screen. Super silly, but hours of work led to this.
      $top_bar_attributes->setAttribute('data-btm-anchor', 9999999);
      $top_bar_attributes->setAttribute('data-margin-top', 0);
      $top_bar_attributes->setAttribute('data-margin-bottom', 0);
      $top_bar_attributes->setAttribute('data-options', 'stickyOn:small');
      $variables['top_bar_sticky'] = TRUE;
    }

    if ($variables['top_bar'] == 2) {
      $top_bar_attributes->addClass('hide-for-medium');
    }

    if (!empty(theme_get_setting('zurb_template_top_bar_grid'))) {
      $top_bar_attributes->addClass('grid-container');
    }

    $variables['top_bar_attributes'] = $top_bar_attributes;
    $variables['top_bar_menu_text'] = theme_get_setting('zurb_template_top_bar_menu_text');

    if (!theme_get_setting('zurb_template_top_bar_is_hover')) {
      $variables['attributes']['data-disable-hover'] = 'true';
    }

    // Format the linked site name.
    $site_name = \Drupal::config('system.site')->get('name');
    $title = strip_tags($site_name) . ' ' . t('Home');
    $url = Url::fromRoute('<front>');
    $url->setOption('attributes', array('title' => $title));

    $variables['linked_site_name'] = '';
    if (!empty($variables['site_name'])) {
      $variables['linked_site_name'] = Link::fromTextAndUrl($variables['site_name'], $url)->toString();
    }

    $variables['site_name'] = Link::fromTextAndUrl($site_name, $url)->toString();

    // Embed the search form inside the Top Bar.
    if (theme_get_setting('zurb_template_top_bar_search')) {
      if (\Drupal::moduleHandler()->moduleExists('search')) {
        $search_form = \Drupal::formBuilder()
          ->getForm('Drupal\search\Form\SearchBlockForm');

        // Wrap the search form in proper Top Bar classes.
        $search_form['#prefix'] = '<li>';
        $search_form['#suffix'] = '</li>';

        $variables['top_bar_search'] = $search_form;
      }
    }

  }
}

/**
 * Implements hook_preprocess_links().
 */
function zurb_template_preprocess_links(&$variables) {
  // Add button class to link items.
  if (!empty($variables['links'])) {
    foreach ($variables['links'] as $link_key => $link) {
      $link['link']['#options']['attributes']['class'][] = 'button';
      $variables['links'][$link_key] = $link;
    }
  }
}
